// jest.config.js
const esModules = ['swiper'].join('|');
module.exports = {
    collectCoverageFrom: [
        "pages/**/*.{js,jsx}",
        "components/**/*.{js,jsx}",
        "utils/**/*.{js,jsx}",
        "!**/node_modules/**",
        "!**/vendor/**",
        "!**/props.js"
    ],
    rootDir: "../../",
    roots: ["./tests.unit/tests"],
    coverageDirectory: "./tests.unit/coverage",
    moduleNameMapper: {
        // Handle CSS imports (with CSS modules)
        // https://jestjs.io/docs/webpack#mocking-css-modules
        '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',

        // Handle CSS imports (without CSS modules)
        '^.+\\.(css|sass|scss)$': '<rootDir>/tests.unit/mocks/styleMock.js',

        // Handle image imports
        // https://jestjs.io/docs/webpack#handling-static-assets
        '^.+\\.(png|jpg|jpeg|gif|webp|avif|ico|bmp|svg)$/i': `<rootDir>/tests.unit/mocks/fileMock.js`,

        // Handle module aliases
        '^@components/(.*)$': '<rootDir>/components/$1',
        '^@assets(.*)$': '<rootDir>/assets/$1',
        '^@locales(.*)$': '<rootDir>/locales/$1',
        '^@css(.*)$': '<rootDir>/css/$1',
        '^@utils(.*)$': '<rootDir>/utils/$1'

    },
    // Add more setup options before each test is run
    // setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
    testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/.next/'],
    testEnvironment: 'jsdom',
    transform: {
        // Use babel-jest to transpile tests with the next/babel preset
        // https://jestjs.io/docs/configuration#transform-objectstring-pathtotransformer--pathtotransformer-object
        '^.+\\.(js|jsx|ts|tsx)$': ['babel-jest', {presets: ['next/babel']}],
    },
    transformIgnorePatterns: [
        '/node_modules/(?!swiper/)',
        '^.+\\.module\\.(css|sass|scss)$',
    ],
    setupFilesAfterEnv: ["./tests.unit/config/jest.setup.js"],
}
