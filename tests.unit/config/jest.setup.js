// global.ResizeObserver = jest.fn().mockImplementation(() => ({
//     observe: jest.fn(),
//     unobserve: jest.fn(),
//     disconnect: jest.fn(),
// }))

global.ResizeObserver = require('resize-observer-polyfill')

jest.mock('@locales', () => {
      return {
          default: {
              defaultLanguage: 'en',
              languages: ['de', 'en'],
          },
      };
  },
);

jest.mock("@utils/constants",()=>{
    return {
        STORAGE_KEY: {
            "PORTFOLIO": "ZETCOM.zetcom client boilerplate.1649369215.PORTFOLIO",
            "SEARCH_ITEM_ID": "ZETCOM.zetcom client boilerplate.1649369215.SEARCH_ITEM_ID",
            "SEARCH_ITEM_INDEX": "ZETCOM.zetcom client boilerplate.1649369215.SEARCH_ITEM_INDEX",
            "SEARCH_LAST": "ZETCOM.zetcom client boilerplate.1649369215.SEARCH_LAST",
            "SEARCH_SCROLL_TOP": "ZETCOM.zetcom client boilerplate.1649369215.SEARCH_SCROLL_TOP",
        }
    }
})
