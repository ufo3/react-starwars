const Footer = () => {
    return (
        <footer className={'p-4 mt-24 bg-black text-center text-gray-400 opacity-75'}>
            <p>(C) 2021 by Michael Zdolski</p>
        </footer>
    );
};

export default Footer;
